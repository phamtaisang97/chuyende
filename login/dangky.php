<?php
// Include config file
require_once "../database/db.php";

// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Bạn chưa nhập tên đăng nhập ";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Set parameters
            $param_username = trim($_POST["username"]);

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "Tên đăng nhập tồn tại !";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Đã xảy ra lỗi ! vui lòng thử lại ";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Vui lòng nhập mật khẩu !";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Mật khẩu phải 6 ký tự.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Vui lòng xác nhận mật khẩu";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Mật khẩu xác nhận không trùng khớp !! ";
        }
    }

    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){

        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);

            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: dangnhap.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
<div class="layout">
    <div class="wrapper">
    <h2>Đăng ký</h2>
    <p>Tạo 1 tài khoản </p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
            <label>Tên đăng nhập </label>
            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
            <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label>Mật khẩu </label>
            <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
            <label>Nhập lại mật khẩu</label>
            <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
            <span class="help-block"><?php echo $confirm_password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Đăng ký">
        </div>
        <p>Bạn đã có tài khoản ? <a href="dangnhap.php">Đăng nhập ngay </a>.</p>
    </form>
</div>
</div>
<!-- <div class="footer">
    <ul class="danhmuc">
        <li><a href="../vieclam/timviec.php"><i class="fa fa-search" aria-hidden="true"></i>
                <span>Tìm Việc</span>
            </a></li>
        <li><a href="#"><i class="fa fa-blind" aria-hidden="true"></i>
                <span>Tìm thợ</span> </a></li>
        <li><a href="#"><i class="fa fa-car" aria-hidden="true"></i>
                <span>Vận chuyển</span> </a></li>
        <li><a href="#"> <i class="fa fa-map" aria-hidden="true"></i>
                <span>Map</span></a></li>
        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>
                <span>Hồ sơ</span></a></li>
    </ul>
</div> -->
</body>
</html>
<style>
    .wrapper {
        position: absolute;
        left: 50%;
        top: 50%;
        width: 50%;
        transform: translate(-50%,-50%);
    }
    .layout {
        width: 100%;
        height: 100vh;
        background: aliceblue;
    }
</style>