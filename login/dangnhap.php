<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: map/welcome.php");
    exit;
}

// Include config file
require_once "../database/db.php";

// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "Nhập user name ";
    } else{
        $username = trim($_POST["username"]);
    }

    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Nhập mật khẩu ";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, username, password, fullname, birthday, contact, email, address, description FROM users WHERE username = ?";

        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Set parameters
            $param_username = $username;

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);

                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password, $fullname, $birthday, $contact, $email, $address, $description);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();

                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;
                            $_SESSION["fullname"] = $fullname;
                            $_SESSION["birthday"] = $birthday;
                            $_SESSION["contact"] = $contact;
                            $_SESSION["email"] = $email;
                            $_SESSION["address"] = $address;
                            $_SESSION["description"] = $description;

                            // Redirect user to welcome page
                            header("location: ../map/welcome.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "Mật khẩu không đúng nhé !";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "User name không tồn tại ";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper {
            position: absolute;
            left: 50%;
            top: 50%;
            width: 50%;
            transform: translate(-50%,-50%);
        }
        .layout {
            width: 100%;
            height: 100vh;
            background: aliceblue;
        }
    </style>
</head>
<body>
<div class="layout">
    <div class="wrapper">
        <h2>Đăng nhập</h2>
        <p>Điền đầy đủ thông tim </p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Mật khẩu </label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Đăng nhập">
            </div>
            <p>Bạn chưa có tài khoản ? <a href="dangky.php">Đăng ký ngay</a>.</p>
        </form>
    </div>
</div>
<!-- <div class="footer">
    <ul class="danhmuc">
        <li><a href="../vieclam/timviec.php"><i class="fa fa-search" aria-hidden="true"></i>
                <span>Tìm Việc</span>
            </a></li>
        <li><a href="#"><i class="fa fa-blind" aria-hidden="true"></i>
                <span>Tìm thợ</span> </a></li>
        <li><a href="#"><i class="fa fa-car" aria-hidden="true"></i>
                <span>Vận chuyển</span> </a></li>
        <li><a href="../map/welcome.php"> <i class="fa fa-map" aria-hidden="true"></i>
                <span>Map</span></a></li>
        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>
                <span>Hồ sơ</span></a></li>
    </ul>
</div> -->
</body>
</html>