<?php
// Initialize the session
session_start();
require_once "../database/db.php";
$list_categories = "SELECT * FROM categories";
$list_categories = mysqli_query($link, $list_categories);
// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// show notifications
$id = $_SESSION["id"];
$qr_set = "SELECT notifications.status FROM locations, notifications, users
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = users.id AND locations.user_id = $id GROUP BY notifications.id DESC";
$list_set = mysqli_query($link, $qr_set);
$count_notif = 0;
while($row = mysqli_fetch_array($list_set)) {
    if($row['status'] == 1) {
        $count_notif++;
    }
}

?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>APP CÔNG VIỆC</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style>
        .messenger{
            position: fixed;
            top: 15px;
            left: 50%;
            transform: translateX(-50%);
            padding: 15px 20px;
            background: #2caf2c;
            font-size: 14px;
            color: #ffffff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #2caf2c;
            z-index: 1000;
            transition: all 0.5s ease-in-out;
        }
        #messenger1{
            width: 315px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v3"></script> -->
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDX4sSXD8RHHuDN0AwoECksIURo62g7YrY&callback=initMap"></script>
    <script src="../js/1.js"></script>
</head>
<body onload="init();">

<?php if(isset($_SESSION["success"])) : ?>
    <div id="messenger" class="messenger"><?= $_SESSION["success"] ?></div>
<?php endif; ?>
<?php if(isset($_SESSION["success_get_job"])) : ?>
    <div id="messenger1" class="messenger"><?= $_SESSION["success_get_job"] ?></div>
<?php endif; ?>

<div class="form-search">
    <button class="close float-right"><i class="fa fa-times-circle" aria-hidden="true"></i>
    </button>
    <form id="services">
        <div class="form-group">
            <label>Vị trí của bạn</label>
            <input type="text" id="start" class="form-control"/>
        </div>
        <div class="form-group">
            <label>Danh sách loại công việc </label>
            <select class="form-control" id="destination_father" onchange="calculateRoute();">
                <option value="0">tùy chọn</option>
            </select>
        </div>
        <div class="form-group">
            <label>Danh sách công việc </label>
            <select class="form-control" id="destination" onchange="calculateRoute();"></select>
        </div>
        <div class="form-group">
            <input class="form-control submit-chiduong" type="button" value="Chỉ đường" onclick="calculateRoute();"/>
        </div>
    </form>
</div>
<div class="form-group" id="detail">
</div>
<div class="menu_header">
    <ul class="danhmuc">
        <li
        <h1>Xin chào , <b><?php echo htmlspecialchars($_SESSION["fullname"]); ?></b></h1></li>
        <li><a href="../login/dangxuat.php">Đăng xuất</a></li>
    </ul>
    <div class="sapxep" style="padding: 5px;">
        <span style="color: white"><i class="fa fa-filter" aria-hidden="true"></i></span>
        <select name="" id="filter-vitri" onchange="vitriChanged(this)">
            <option value="0">Khu vực</option>
            <option value="1">Hà Nội</option>
            <option value="2">Đà Nẵng</option>
            <option value="3">TP.HCM</option>
        </select>
        <select name="" id="filter-category" onchange="categoryChanged(this)">
            <option data-icon="fa-heart" value="">tất cả việc làm</option>
        <?php
        while($row = mysqli_fetch_array($list_categories))
        {
            ?>
            <option  value="<?= $row['id'] ?>"><?= $row['name_category'] ?></option>
        <?php } ?>
        </select>
    </div>
</div>
<div id="wishlist" class="wishlist">
    <h5>Danh sách việc quan tâm</h5>
</div>
<div class="nut-wishlist"><i class="fa fa-gratipay" aria-hidden="true"></i></div>
<section id="main">
    <div class="container" style="max-width: 100%;padding: 0;">
        <div id="map_canvas" style="width: 100%;height: 100%; position: relative;overflow: hidden;top: 0px;"></div>
    </div>
</section>

<div class="footer">
    <ul class="danhmuc">
        <li><a href="../map/welcome.php">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Home</span>
        </a></li>
        <li><a href="#" class="tim_viec">
            <i class="fa fa-search" aria-hidden="true"></i>
            <span>Tìm Việc</span>
        </a></li>
        <li><a href="../vieclam/themmoi.php">
            <i class="fa fa-plus" aria-hidden="true"></i>
            <span>Thêm việc</span>
        </a></li>
        <li style="position: relative;"><a href="../vieclam/danhsach.php">
            <i class="fa fa-list" aria-hidden="true"></i>
            <span>Danh sách</span>
            <div class="<?= $count_notif != 0 ? 'notifications' : '' ?>"><?= $count_notif != 0 ? $count_notif : '' ?></div>
        </a></li>
        <li><a href="../user/profile.php">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span>Hồ sơ</span>
        </a></li>
        <li><a href="../user/change_password.php">
            <i class="fa fa-key" aria-hidden="true"></i>
            <span>Đổi mật khẩu</span>
        </a></li>
    </ul>
</div>
<script>
    $( ".nut-wishlist" ).click(function() {
        $('.wishlist').toggleClass("highlight");
    });
</script>
<script>
    let mess = document.getElementById('messenger');
    if(mess) {
        setTimeout(function() {
            mess.style.top = '-80px';
        }, 2500);
    }
    let mess1 = document.getElementById('messenger1');
    if(mess1) {
        setTimeout(function() {
            mess1.style.top = '-100px';
        }, 4000);
    }
</script>
</body>
</html>

<?php
   unset($_SESSION['success']);
   unset($_SESSION['success_get_job']);
?>