<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

$success = '';

$birthday = explode('/', $_SESSION["birthday"]);
if (isset($_SESSION["birthday"]) && $_SESSION["birthday"] != ''){
    $date = $birthday[0];
    $month = $birthday[1];
    $year = $birthday[2];
}
else {
    $date = date('d');
    $month = date('m');
    $year = date('Y');
}

// Include config file
require_once "../database/db.php";

if(isset($_POST['btn_save'])) {
	$id = $_SESSION["id"];
    $fullname = $_POST["fullname"];
    $fullname_khongdau = utf8convert($_POST["fullname"]);
    $date = $_POST["date"];
    $month = $_POST["month"];
    $year = $_POST["year"];
    $birthday = $date . '/' . $month . '/' . $year;
    $contact = $_POST["contact"];
    $email = $_POST["email"];
    $address = $_POST["address"];
    $description = $_POST["description"];

    $qr = "
        UPDATE users SET
        fullname = '$fullname',
        fullname_khongdau = '$fullname_khongdau',
        birthday = '$birthday',
        contact = '$contact',
        email = '$email',
        address = '$address',
        description = '$description'
        WHERE id = '$id'
    ";
	mysqli_set_charset($link, "utf8");
	mysqli_query($link, $qr);

    $_SESSION["fullname"] = $fullname;
    $_SESSION["birthday"] = $birthday;
    $_SESSION["contact"] = $contact;
    $_SESSION["email"] = $email;
    $_SESSION["address"] = $address;
    $_SESSION["description"] = $description;
    
    $success = 'Lưu thành công !';
}

// show notifications
$id = $_SESSION["id"];
$qr_set = "SELECT notifications.status FROM locations, notifications, users
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = users.id AND locations.user_id = $id GROUP BY notifications.id DESC";
$list_set = mysqli_query($link, $qr_set);
$count_notif = 0;
while($row = mysqli_fetch_array($list_set)) {
    if($row['status'] == 1) {
        $count_notif++;
    }
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Profile</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome.css">
	<link rel="stylesheet" href="../css/style.css">
    <style>        
        #messenger{
            position: fixed;
            top: 15px;
            right: 15px;
            padding: 15px 20px;
            background: #2caf2c;
            font-size: 14px;
            color: #ffffff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #2caf2c;
            z-index: 1000;
            transition: all 0.5s ease-in-out;
        }
    </style>
</head>
<body>
    <?php if($success != '') : ?>
        <div id="messenger"><?= $success ?></div>
    <?php endif; ?>
	<div class="menu_header">
	    <ul class="danhmuc">
	        <li
	        <h1>Xin chào , <b><?= $_SESSION["fullname"] ?></b></h1></li>
	        <li><a href="../login/dangxuat.php">Đăng xuất</a></li>
	    </ul>
	</div>
    <section class="hottest-house buy-house-1 bgf6 edit-info">
        <div class="container">
            <div class="row rela">
                <div class="col-12">
                    <div class="basic-info">
                        <div class="title-basic text-center">
                            <div class="img-basic">
                                <img src="../images/mr_tuan.jpg" alt="" id="avatar">
                                <label>
                                    <input type="file" name="" id="choose-img" class="cspoint">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </div>
                            <h1>thông tin cơ bản</h1>
                        </div>
                        <form action="" method="POST">
                            <div class="row mg-5">
                                <label class="col-12 pd5">
                                    <h3>Họ tên</h3>
                                    <input type="text" name="fullname" class="w-100" value="<?= isset($_SESSION["fullname"]) ? $_SESSION["fullname"] : '' ?>">
                                </label>
                                <div class="col-12 pd5 marbot15">
                                    <div class="row mg-5">
                                        <div class="col-4 pd5">
                                        	<h3>Ngày</h3>
                                            <select name="date" id="">
                                            	<?php for($i = 1; $i <= 31; $i++) : ?>
                                                	<option <?= $i == $date ? 'selected' : '' ?> value="<?= $i < 10 ? '0'.$i : $i ?>"><?= $i < 10 ? '0'.$i : $i ?></option>
												<?php endfor; ?>
                                            </select>
                                        </div>
                                        <div class="col-4 pd5">
                                        	<h3>Tháng</h3>
                                            <select name="month" id="">
                                            	<?php for($i = 1; $i <= 12; $i++) : ?>
                                                	<option <?= $i == $month ? 'selected' : '' ?> value="<?= $i < 10 ? '0'.$i : $i ?>"><?= $i < 10 ? '0'.$i : $i ?></option>
												<?php endfor; ?>
                                            </select>
                                        </div>
                                        <div class="col-4 pd5">
                                        	<h3>Năm</h3>
                                            <select name="year" id="">
                                            	<?php for($i = 1970; $i <= 2020; $i++) : ?>
                                                	<option <?= $i == $year ? 'selected' : '' ?> value="<?= $i ?>"><?= $i ?></option>
												<?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <label class="col-sm-4 col-12 pd5">
                                    <h3>Số điện thoại</h3>
                                    <input type="text" name="contact" class="w-100" value="<?= isset($_SESSION["contact"]) ? $_SESSION["contact"] : '' ?>">
                                </label>
                                <label class="col-sm-4 col-12 pd5">
                                    <h3>Email</h3>
                                    <input type="text" name="email" class="w-100" value="<?= isset($_SESSION["email"]) ? $_SESSION["email"] : '' ?>">
                                </label>                           
                                <label class="col-sm-4 col-12 pd5">
                                    <h3>Địa chỉ</h3>
                                    <input type="text" name="address" class="w-100" value="<?= isset($_SESSION["address"]) ? $_SESSION["address"] : '' ?>">
                                </label>
                                <label class="col-12 pd5">
                                    <h3>Mô tả / Chức vụ / Doanh nghiệp</h3>
                                    <textarea name="description" id="" cols="" rows="2" class="w-100"><?= isset($_SESSION["description"]) ? $_SESSION["description"] : '' ?></textarea>
                                </label>
                            </div>
                            <p class="text-center"><button type="submit" name="btn_save" class="btn-save">Lưu</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="footer">
        <ul class="danhmuc">
            <li><a href="../map/welcome.php">
                <i class="fa fa-home" aria-hidden="true"></i>
                <span>Home</span>
            </a></li>
            <li><a href="../vieclam/themmoi.php">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span>Thêm việc</span>
            </a></li>
            <li style="position: relative;"><a href="../vieclam/danhsach.php">
                <i class="fa fa-list" aria-hidden="true"></i>
                <span>Danh sách</span>
                <div class="<?= $count_notif != 0 ? 'notifications' : '' ?>"><?= $count_notif != 0 ? $count_notif : '' ?></div>
            </a></li>
            <li><a href="../user/profile.php">
                <i class="fa fa-user" aria-hidden="true"></i>
                <span>Hồ sơ</span>
            </a></li>
            <li><a href="../user/change_password.php">
                <i class="fa fa-key" aria-hidden="true"></i>
                <span>Đổi mật khẩu</span>
            </a></li>
        </ul>
    </div>
    <script>
        let mess = document.getElementById('messenger');
        if(mess) {
            setTimeout(function() {
                mess.style.top = '-80px';
            }, 2500);
        }
    </script>
</body>
</html>