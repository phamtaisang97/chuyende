<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

$success = $password = $repeat_password = "";
$password_err = $repeat_password_err = "";

if(isset($_POST['btn_save'])) {
	$id = $_SESSION["id"];
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Vui lòng nhập mật khẩu !";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Mật khẩu phải 6 ký tự.";
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if(empty(trim($_POST["repeat_password"]))){
        $repeat_password_err = "Vui lòng xác nhận mật khẩu";
    } else{
        $repeat_password = trim($_POST["repeat_password"]);
        if(empty($password_err) && ($password != $repeat_password)){
            $repeat_password_err = "Mật khẩu xác nhận không trùng khớp !! ";
        }
    }

    if(empty($password_err) && empty($confirm_password_err)){
        $success = 'Đổi mật khẩu thành công !';
        $param_password = password_hash($password, PASSWORD_DEFAULT); 

        $qr = "
            UPDATE users SET
            password = '$param_password'
            WHERE id = '$id'
        ";
    	mysqli_set_charset($link, "utf8");
    	mysqli_query($link, $qr);
    }
}

// show notifications
$id = $_SESSION["id"];
$qr_set = "SELECT notifications.status FROM locations, notifications, users
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = users.id AND locations.user_id = $id GROUP BY notifications.id DESC";
$list_set = mysqli_query($link, $qr_set);
$count_notif = 0;
while($row = mysqli_fetch_array($list_set)) {
    if($row['status'] == 1) {
        $count_notif++;
    }
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Đổi mật khẩu</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome.css">
	<link rel="stylesheet" href="../css/style.css">
    <style>
        .help-block{
            margin: -5px 0 9px 5px !important;
            font-size: 12px;
            color: red;
        }
        #messenger{
            position: fixed;
            top: 15px;
            right: 15px;
            padding: 15px 20px;
            background: #2caf2c;
            font-size: 14px;
            color: #ffffff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #2caf2c;
            z-index: 1000;
            transition: all 0.5s ease-in-out;
        }
    </style>
</head>
<body>
    <?php if($success != '') : ?>
        <div id="messenger"><?= $success ?></div>
    <?php endif; ?>
	<div class="menu_header">
	    <ul class="danhmuc">
	        <li
	        <h1>Xin chào , <b><?= $_SESSION["fullname"] ?></b></h1></li>
	        <li><a href="../login/dangxuat.php">Đăng xuất</a></li>
	    </ul>
	</div>
    <section class="hottest-house buy-house-1 bgf6 edit-info">
        <div class="container">
            <div class="row rela">
                <div class="col-12">
                    <div class="basic-info">
                        <div class="title-basic text-center">
                            <div class="img-basic">
                                <img src="../images/mr_tuan.jpg" alt="" id="avatar">
                                <label>
                                    <input type="file" name="" id="choose-img" class="cspoint">
                                    <i class="fa fa-pencil"></i>
                                </label>
                            </div>
                            <h1>Đổi mật khẩu</h1>
                        </div>
                        <form action="" method="POST">
                            <div class="row mg-5">
                                <label class="col-12 pd5">
                                    <h3>Tài khoản</h3>
                                    <input type="text" class="w-100" value="<?= isset($_SESSION["username"]) ? $_SESSION["username"] : '' ?>" disabled>
                                </label>
                                <label class="col-12 pd5">
                                    <h3>Mật khẩu mới</h3>
                                    <input type="password" name="password" class="w-100" value="">
                                </label>
                                <?php if($password_err != '') : ?>
                                    <span class="help-block"><?= $password_err ?></span>
                                <?php endif; ?>
                                <label class="col-12 pd5">
                                    <h3>Nhập lại mật khẩu mới</h3>
                                    <input type="password" name="repeat_password" class="w-100" value="">
                                </label>
                                <?php if($repeat_password_err != '') : ?>
                                    <span class="help-block"><?= $repeat_password_err ?></span>
                                <?php endif; ?>
                            </div>
                            <p class="text-center"><button type="submit" name="btn_save" class="btn-save">Lưu</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="footer">
        <ul class="danhmuc">
            <li><a href="../map/welcome.php">
                <i class="fa fa-home" aria-hidden="true"></i>
                <span>Home</span>
            </a></li>
            <li><a href="../vieclam/themmoi.php">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span>Thêm việc</span>
            </a></li>
            <li style="position: relative;"><a href="../vieclam/danhsach.php">
                <i class="fa fa-list" aria-hidden="true"></i>
                <span>Danh sách</span>
                <div class="<?= $count_notif != 0 ? 'notifications' : '' ?>"><?= $count_notif != 0 ? $count_notif : '' ?></div>
            </a></li>
            <li><a href="../user/profile.php">
                <i class="fa fa-user" aria-hidden="true"></i>
                <span>Hồ sơ</span>
            </a></li>
            <li><a href="../user/change_password.php">
                <i class="fa fa-key" aria-hidden="true"></i>
                <span>Đổi mật khẩu</span>
            </a></li>
        </ul>
    </div>

    <script>
        let mess = document.getElementById('messenger');
        if(mess) {
            setTimeout(function() {
                mess.style.top = '-80px';
            }, 2500);
        }
    </script>
</body>
</html>