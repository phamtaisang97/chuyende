    function makeRequest(url, callback) {
        var request;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest(); // IE7+, Firefox, Chrome, Opera, Safari
        } else {
            request = new ActiveXObject("Microsoft.XMLHTTP"); // IE6, IE5
        }
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                callback(request);
            }
        }
        request.open("GET", url, true);
        request.send();
    }

var map;

var center = new google.maps.LatLng(21.0228161, 105.801944); //21.0717007,105.7717092
var geocoder = new google.maps.Geocoder();
var infowindow = new google.maps.InfoWindow();

var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();

function init() {
    var mapOptions = {
        zoom: 10,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    //lay vi tri user
    $('.close').click(function () {
        $('.form-search').removeClass("show");
    });
    $('.tim_viec').click(function () {
        $('.form-search').addClass("show");
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {

                var userLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                geocoder.geocode({'latLng': userLocation}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        document.getElementById('start').value = results[0].formatted_address
                    }
                });

            }, function () {
                alert('Geolocation is supported, but it failed');
            });
        }
        $('.submit-chiduong').click(function () {
            $('.form-search').removeClass("show");
        });
    });

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('directions_panel'));
    wishlist();
    var option = $( "#filter-category option:selected" ).val();
    console.log(option);
    if (option == 0){
        get_all();
    }
}
    addOption_father();
function vitriChanged(obj) {
    var value = obj.value;
    if (value == 1){
        center = new google.maps.LatLng(21.0228161, 105.801944);
        init();
    }if (value == 2){
        center = new google.maps.LatLng(16.0472484, 108.1716866);
        init();
    }
    if (value == 3){
        center = new google.maps.LatLng(10.7631568, 106.7106498);
        init();
    }
    init();
}
console.log(center);
//bắt sự kiện select option của category map
function categoryChanged(obj)
{
    var id = obj.value;
   if (id == 0){
       get_all();
   }
   else {
    filter(id);

   }
   init();
}
//loc tim kiem
function filter(id) {
makeRequest('../database/get_locations.php?id='+id, function (data) {
    var data = JSON.parse(data.responseText);
    for (var i = 0; i < data.length; i++) {
        console.log(data[i]);
        displayLocation(data[i]);
    }
});
}
function get_all() {
    makeRequest('../database/get_locations.php', function (data) {
        var data = JSON.parse(data.responseText);
        for (var i = 0; i < data.length; i++) {
            displayLocation(data[i]);
        }
    });
}
//wishlist
function wishlist() {
    makeRequest('../database/get_wishlist.php', function (data) {

        var data = JSON.parse(data.responseText);

        var wishlist = document.getElementById('wishlist');

        for (var i = 0; i < data.length; i++) {
           console.log(data[i]['name']);
            $( "#wishlist" ).append( "<div class='tt-wish'><p>tên :"+data[i]['name']+"</p>"
                + "<p>Địa chỉ :"+data[i]['address']+"</p>"
                +"<p style='color:yellow;'>Lương :"+data[i]['price']+"VNĐ</p><p><a style='color: black' href='#'>Nhận việc ngay</a></p>"
                +"<p><a href='../database/delete_wishlist.php?id="+data[i]['id']+"'>Hủy</a></p></div>"
            );
        }
    });
}

// chỉ đường call data
function addOption_father() {
    makeRequest('../database/get_categories.php', function (data) {

        var data = JSON.parse(data.responseText);

        var selectBox_father = document.getElementById('destination_father');

        for (var i = 0; i < data.length; i++) {
            addOption(selectBox_father, data[i]['name_category'], data[i]['id']);
        }

        $(selectBox_father).change(function(){
            var selectedCountry = $(this).children("option:selected").val();
            makeRequest('../database/get_locations.php', function (data) {
                var data = JSON.parse(data.responseText);
                var selectBox = document.getElementById('destination');
                for (var i = 0; i < data.length; i++) {
                    if (selectedCountry == data[i]['category_id']){
                        addOption(selectBox, data[i]['name'], data[i]['address'], data[i]['price']);
                    }
                }
            });
            init();
        });
    });

}

function addOption(selectBox, text, value, price) {
    var option = document.createElement("OPTION");
    option.text = text;
    option.value = value;
    if (price){
        $(option).append('<span>   ('+price+' VNĐ)</span>');
    }
    selectBox.options.add(option);
}

//login chỉ đường  tính toán định tuyến
function calculateRoute() {

    var start = document.getElementById('start').value;
    var destination = document.getElementById('destination').value;

    if (start == '') {
        start = center;
    }

    var request = {
        origin: start,
        destination: destination,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            //đặt chỉ đường nhé
            directionsDisplay.setDirections(response);
            //thông tin chi tiết tuyến đg
            var khoang_cach = response.routes[0].legs[0].distance.text;
            var thoi_gian = response.routes[0].legs[0].duration.text;
            $('#detail').append('<ul><li>Khoảng cách : '+khoang_cach+'</li><li>Thời gian ước tính : '+thoi_gian+'</li></ul>')
        }
    });
}

var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

//vẽ map
function displayLocation(location) {
    if (location.vote_star == null || location.vote_star == 0) {
        var content = '<p class="name">Tên công việc: ' + location.name + '</p>'
        + '<p class="address">Địa điểm: ' + location.address + '</p>'
        + '<p class="time">Thời gian: ' + location.time_work + '</p>'
        + '<p>Lương: <span class="price">' + location.price + ' VNĐ</span></p>'
        + '<p class="mota">Mô tả công việc: ' + location.description + '</p>'
        + '<p>Liên hệ: ' + location.fullname_khongdau + ' <a href="tel:' + location.contact + '" class="lienhe">(' + location.contact + ')</a></p>'
        + `<p>Đánh giá: Chưa có đánh giá nào</p>`
        + '<form action="../database/wishlist.php" method="post">' +
        '<input type="hidden" name="id" value='+location.id+'>' +
        '<div class="d-flex"><button type="submit" class="btn btn-info">Quan tâm </button>' +
        '</form>'
        + '<a href="../vieclam/nhanviec.php?id_job=' + location.id + '" class="btn btn-danger detail">Nhận việc ngay</a></div>';
    }
    else {
        var content = '<p class="name">Tên công việc: ' + location.name + '</p>'
            + '<p class="address">Địa điểm: ' + location.address + '</p>'
            + '<p class="time">Thời gian: ' + location.time_work + '</p>'
            + '<p>Lương: <span class="price">' + location.price + ' VNĐ</span></p>'
            + '<p class="mota">Mô tả công việc: ' + location.description + '</p>'
            + '<p>Liên hệ: ' + location.fullname_khongdau + ' <a href="tel:' + location.contact + '" class="lienhe">(' + location.contact + ')</a></p>'
            + `<div class="d-flex align-items-center"><p style="margin-right: 5px;">Đánh giá:</p><div class="star-rating">
                    <div class="star-base">
                        <div class="star-rate" style="width:${location.vote_star}%"></div>
                        <a dt-value="1" id="hover-1" href="#1" title="1 sao"></a>
                        <a dt-value="2" id="hover-2" href="#2" title="2 sao"></a>
                        <a dt-value="3" id="hover-3" href="#3" title="3 sao"></a>
                        <a dt-value="4" id="hover-4" href="#4" title="4 sao"></a>
                        <a dt-value="5" id="hover-5" href="#5" title="5 sao"></a>
                    </div>
                </div><p> &nbsp;(${parseInt(location.vote_count) + parseInt(location.total_vote)} đánh giá)</p></div>`
            + '<form action="../database/wishlist.php" method="post">' +
            '<input type="hidden" name="id" value='+location.id+'>' +
            '<div class="d-flex"><button type="submit" class="btn btn-info">Quan tâm </button>' +
            '</form>'
            + '<a href="../vieclam/nhanviec.php?id_job=' + location.id + '" class="btn btn-danger detail">Nhận việc ngay</a></div>';
    }

    //
    // makeRequest('../database/get_wishlist.php', function (data) {
    //     var data = JSON.parse(data.responseText);
    //     for (var i = 0; i < data.length; i++) {
    //         if (location.id == data[i]['location_id']){
    //             icon = 1;
    //         }
    //     }
    // });

    //console.log("2");

    if (parseInt(location.lat) == 0) {
        geocoder.geocode({'address': location.address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    icon: iconBase + 'parking_lot_maps.png',
                    title: location.name
                });

                // // Save geocoding result to the Database
                // var url = 'set_coords.php?id=' + location.id
                //     + '&amp;lat=' + results[0].geometry.location.lat()
                //     + '&amp;lon=' + results[0].geometry.location.lng();
                //
                // makeRequest(url, function (data) {
                //     if (data.responseText == 'OK') {
                //         // Succes
                //     }
                // });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                });
            }
        });
    } else {
        //pos == 0 ? position = new google.maps.LatLng(parseFloat(location.lat), parseFloat(location.lon)) : "";
        var position = new google.maps.LatLng(parseFloat(location.lat), parseFloat(location.lon));
        //location.vote_star ? url_icon = "../images/star.png" : url_icon = "../images/mark.png";
        if (location.category_id == 1){
            url_icon = "../images/office.png"
        }
        if (location.category_id == 2){
            url_icon = "../images/clear.png"
        }
        if (location.category_id == 3){
            url_icon = "../images/setting.png"
        }

        var icon = {
            url: '../images/mark.png', // url
            scaledSize: new google.maps.Size(40, 40), // scaled size
        };
        var marker = new google.maps.Marker({
            map: map,
            position: position,
            icon: icon,
            title: location.name
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(content);
            infowindow.open(map, marker);
        });
    }

}
