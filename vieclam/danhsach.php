<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

$id = $_SESSION["id"];

$qr_post = "SELECT * FROM locations WHERE user_id = $id";
$list_post = mysqli_query($link, $qr_post);

$qr_take = "SELECT locations.*, notifications.id as notif_id, notifications.status, notifications.vote_star_user_get FROM locations, notifications
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = $id GROUP BY notifications.id DESC";
$list_take = mysqli_query($link, $qr_take);

$qr_set = "SELECT locations.*, notifications.id as notif_id, notifications.status, users.fullname, users.id as id_user FROM locations, notifications, users
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = users.id AND locations.user_id = $id GROUP BY notifications.id DESC";
$list_set = mysqli_query($link, $qr_set);

// show notifications
$qr_check = "SELECT notifications.status FROM locations, notifications, users
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = users.id AND locations.user_id = $id GROUP BY notifications.id DESC";
$qr_check = mysqli_query($link, $qr_check);
$count_notif = 0;
while($row = mysqli_fetch_array($qr_check)) {
    if($row['status'] == 1) {
        $count_notif++;
    }
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Danh sách công việc</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome.css">
	<link rel="stylesheet" href="../css/style.css">
    <style>
        #messenger{
            position: fixed;
            top: 15px;
            right: 15px;
            padding: 15px 20px;
            background: #2caf2c;
            font-size: 14px;
            color: #ffffff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #2caf2c;
            z-index: 1000;
            transition: all 0.5s ease-in-out;
        }
        #messenger_change{
            position: fixed;
            top: -80px;
            right: 15px;
            padding: 15px 20px;
            background: #2caf2c;
            font-size: 14px;
            color: #ffffff;
            border-radius: 5px;
            box-shadow: 0px 0px 10px 0px #2caf2c;
            z-index: 1000;
            transition: all 0.5s ease-in-out;
        }
        #messenger_change.active{
            top: 15px;
        }
        h1{
            margin: 15px 0;
            font-size: 18px;
            text-align: center;
        }
        table{
            width: 100%;
            border: 1px solid #ddd;
            border-collapse: collapse;
            font-size: 12px;
        }
        th, td{
            padding: 2px 5px;
            border: 1px solid #ddd;
        }
        th{
            background: #a3a3a3;
        }
        tr:nth-child(odd){
            background: #f6f6f6;
        }
        th:last-child, td:last-child{
            min-width: 35px;
        }
        select{
            padding: 1px 0;
            border: 1px solid #000;
        }
        .btn-vote{
            display: block;
            width: 65px;
            margin: 3px 0;
            padding: 5px 0;
            border: none;
            border-radius: 3px;
            color: #fff;
            background: #009688;
        }
        .nav-tabs{
            margin-top: 10px;
        }
        .nav-link{
            padding: 8px;
            font-size: 14px;
            color: #495057;
        }
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active{
            color: #fff;
            background: #009688;
        }
        #showJob, #showVoteJob, #showVoteUser{            
            /*margin-top: 70px;*/
            transform: scale(0);
            transition: all 0.2s ease-in-out;
        }
        .show-job-detail, .show-vote{
            position: fixed;
            top: 40%;
            left: 50%;
            max-width: 95%;
            min-width: 280px;
            transform: translate(-50%, -50%);
            background: #009688;
            color: #fff;
            padding: 10px 15px;
            border-radius: 5px;
            box-shadow: 0 1px 5px 0px #009688;
            z-index: 999;
        }
        .show-vote{
            font-size: 14px;
        }
        .show-job-detail h5{
            text-align: center;
        }
        .show-job-detail div{
            font-size: 14px;
        }
        .show-job-detail a{
            color: #fff;
            text-decoration: underline;
        }
        .show-job-detail .btn-close-job, .show-vote .btn-close-vote-job, .show-vote .btn-close-vote-user{
            position: absolute;
            top: 0;
            right: 10px;
            font-size: 24px;
            cursor: pointer;
        }
        .show-job-detail .d-flex span{
            margin-right: 10px;
        }
        .show-user, .show-job{
            color: #009688;
            cursor: pointer;
        }


        .label-act, .not-change-status, .not-permission{
            position: relative;
            width: 40px;
            height: 20px;
        }
        .in-act{
            display: none;
        }
        .label-act .bg-unact, .not-change-status .bg-unact, .not-permission .bg-unact{
            position: absolute;
            top: 0; 
            left: 0; 
            width: 40px; 
            height: 20px; 
            border: none; 
            box-shadow: 2px 0px 5px #cccccc; 
            border-radius: 50px; 
            border: 0.5px solid #cccccc;
            background: #dddddd; 
            cursor: pointer;
            transition: all 0.3s ease-in-out;
        }
        .label-act .bg-act, .not-change-status .bg-act, .not-permission .bg-act{
            position: absolute; 
            top: 0; 
            left: 0; 
            width: 40px; 
            height: 20px; 
            border: none; 
            box-shadow: 2px 0px 5px #cccccc;
            border-radius: 50px; 
            background: #28a745; 
            cursor: pointer;
            transition: all 0.3s ease-in-out;
        }
        .label-act .bg-unact .btn-unact, .not-change-status .bg-unact .btn-unact, .not-permission .bg-unact .btn-unact{
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0; 
            width: 20px; 
            height: 20px; 
            background: #fff; 
            border-radius: 45%;
            transition: all 0.2s ease-in-out;
        }
        .label-act .bg-act .btn-act, .not-change-status .bg-act .btn-act, .not-permission .bg-act .btn-act{
            position: absolute; 
            top: 0; 
            bottom: 0; 
            left: 20px; 
            width: 20px; 
            height: 20px; 
            background: #fff; 
            border-radius: 45%;
            transition: all 0.2s ease-in-out;
        }
    </style>
</head>
<body>

    <?php if(isset($_SESSION["success"])) : ?>
        <div id="messenger"><?= $_SESSION["success"] ?></div>
    <?php endif; ?>

	<div class="menu_header">
	    <ul class="danhmuc">
	        <li
	        <h1>Xin chào , <b><?= $_SESSION["fullname"] ?></b></h1></li>
	        <li><a href="../login/dangxuat.php">Đăng xuất</a></li>
	    </ul>
	</div>

    <div class="container">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#job_post">Việc đã đăng</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#job_take">Việc đã nhận</a>
            </li>
            <li class="nav-item" style="position: relative;">
                <a class="nav-link" data-toggle="tab" href="#job_update">Xét duyệt</a>
                <div class="change-notif <?= $count_notif != 0 ? 'notifications' : '' ?>" style="top: -5px; right: -5px;"><?= $count_notif != 0 ? $count_notif : '' ?></div>
            </li>
        </ul>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane container active" id="job_post">
            <h1>Danh sách công việc đã đăng</h1>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên CV</th>
                        <th>Thời gian</th>
                        <th>Địa điểm</th>
                        <th>Lương</th>
                        <th>Trạng thái</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        while($row = mysqli_fetch_array($list_post))
                        {
                            ob_start();
                    ?>
                        <tr data-id-job="<?= $row['id'] ?>">
                            <td><?= $i ?></td>
                            <td>{name}</td>
                            <td>{time_work}</td>
                            <td>{address}</td>
                            <td>{price}đ</td>
                            <td>
                                <?php if($row['status'] == 1) : ?>
                                    <label class="label-act admin-status">
                                        <input class="in-act" type="checkbox" name="" value="1" checked >
                                        <div class="bg bg-act" title="Đã kích hoạt">
                                            <div class="button btn-act"></div>
                                        </div>
                                    </label>
                                <?php else : ?>
                                    <label class="label-act admin-status">
                                        <input class="in-act" type="checkbox" name="" value="0" >
                                        <div class="bg bg-unact" title="Chưa kích hoạt">                                    
                                            <div class="button btn-unact"></div>
                                        </div>
                                    </label>
                                <?php endif; ?>
                            </td>
                            <td>
                                <a href="edit.php?id={id}" title=""><i class="fa fa-pencil"></i></a>
                                <a href="delete.php?id={id}" onclick="confirm('Bạn có chắc muốn xóa không?');" title=""><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php
                            $s = ob_get_clean();
                            $s = str_replace("{id}", $row['id'], $s);
                            $s = str_replace("{name}", $row['name'], $s);
                            $s = str_replace("{time_work}", $row['time_work'], $s);
                            $s = str_replace("{address}", $row['address'], $s);
                            $s = str_replace("{price}", $row['price'], $s);
                            $i++;
                            echo $s;
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane container fade" id="job_take">
            <h1>Danh sách công việc đã nhận</h1>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên CV</th>
                        <th>Lương</th>
                        <th>Trạng thái</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        while($row = mysqli_fetch_array($list_take))
                        {
                    ?>
                        <tr data-id-job="<?= $row['id'] ?>" data-notif-id="<?= $row['notif_id'] ?>" data-notif-status="<?= $row['status'] ?>">
                            <td><?= $i ?></td>
                            <td class="show-job"><?= $row['name'] ?></td>
                            <td><?= $row['price'] ?>đ</td>
                            <td><?= $row['status'] == 0 ? 'Bị từ chối' : ($row['status'] == 1 ? 'Chờ phê duyệt' : ($row['status'] == 2 ? 'Đã phê duyệt' : 'Đã hoàn thành')) ?></td>
                            <td class="job-voted-<?= $row['id'] ?>">
                                <?php if($row['vote_star_user_get'] != 0) : ?>
                                    <div class="star-rating">
                                        <div class="star-base">
                                            <div class="star-rate" style="width:<?= $row['vote_star_user_get'] ?>%"></div>
                                            <a dt-value="1" href="#1" title="1 sao"></a>
                                            <a dt-value="2" href="#2" title="2 sao"></a>
                                            <a dt-value="3" href="#3" title="3 sao"></a>
                                            <a dt-value="4" href="#4" title="4 sao"></a>
                                            <a dt-value="5" href="#5" title="5 sao"></a>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <button class="btn-vote">Đánh giá</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php
                            $i++;
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane container fade" id="job_update">
            <h1>Danh sách công việc được nhận</h1>
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Người nhận</th>
                        <th>Tên CV</th>
                        <th>Lương</th>
                        <th>Trạng thái</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        while($row = mysqli_fetch_array($list_set))
                        {
                    ?>
                        <tr data-id-user="<?= $row['id_user'] ?>" data-notif-id="<?= $row['notif_id'] ?>">
                            <td><?= $i ?></td>
                            <td class="show-user"><?= $row['fullname'] ?></td>
                            <td><?= $row['name'] ?></td>
                            <td><?= $row['price'] ?>đ</td>
                            <td>
                                <select class="change-status">
                                    <option value="0" <?= $row['status'] == 0 ? 'selected' : '' ?>>Từ chối</option>
                                    <option value="1" <?= $row['status'] == 1 ? 'selected' : '' ?>>Chờ phê duyệt</option>
                                    <option value="2" <?= $row['status'] == 2 ? 'selected' : '' ?>>Phê duyệt</option>
                                    <option value="3" <?= $row['status'] == 3 ? 'selected' : '' ?>>Đã hoàn thành</option>
                                </select>
                            </td>                            
                        </tr>
                    <?php
                            $i++;
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="footer">
        <ul class="danhmuc">
            <li><a href="../map/welcome.php">
                <i class="fa fa-home" aria-hidden="true"></i>
                <span>Home</span>
            </a></li>
            <li><a href="../vieclam/themmoi.php">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span>Thêm việc</span>
            </a></li>
            <li style="position: relative;"><a href="../vieclam/danhsach.php">
                <i class="fa fa-list" aria-hidden="true"></i>
                <span>Danh sách</span>
                <div class="change-notif <?= $count_notif != 0 ? 'notifications' : '' ?>"><?= $count_notif != 0 ? $count_notif : '' ?></div>
            </a></li>
            <li><a href="../user/profile.php">
                <i class="fa fa-user" aria-hidden="true"></i>
                <span>Hồ sơ</span>
            </a></li>
            <li><a href="../user/change_password.php">
                <i class="fa fa-key" aria-hidden="true"></i>
                <span>Đổi mật khẩu</span>
            </a></li>
        </ul>
    </div>
    
    <div id="showJob"></div>

    <div id="messenger_change">Cập nhật thành công</div>

    <div id="showVoteJob">
        <div class="show-vote">
            <div class="btn-close-vote-job"><i class="fa fa-times-circle"></i></div>
            <div>Mời bạn đánh giá sao cho công việc này với mức độ hài lòng của bạn với công việc và người đăng việc.</div>
            <div>Tương ứng từ 1 đến 5 sao:</div>
            <div class="star-rating">
                <div class="star-base">
                    <div id="star_rate_job" class="star-rate" style="width:100%"></div>
                    <a dt-value="1" id="hover-1" href="#1" title="1 sao"></a>
                    <a dt-value="2" id="hover-2" href="#2" title="2 sao"></a>
                    <a dt-value="3" id="hover-3" href="#3" title="3 sao"></a>
                    <a dt-value="4" id="hover-4" href="#4" title="4 sao"></a>
                    <a dt-value="5" id="hover-5" href="#5" title="5 sao"></a>
                </div>
            </div>
            <button type="button" class="btn btn-danger btn-vote-submit" data-job-id="" data-vote="5" >Đánh giá</button>
        </div>
    </div>

    <div id="showVoteUser">
        <div class="show-vote">
            <div class="btn-close-vote-user"><i class="fa fa-times-circle"></i></div>
            <div>Mời bạn đánh giá sao cho user này với mức độ hài lòng của bạn với user này.</div>
            <div>Tương ứng từ 1 đến 5 sao:</div>
            <div class="star-rating">
                <div class="star-base">
                    <div id="star_rate_user" class="star-rate" style="width:100%"></div>
                    <a dt-value="1" id="click-1" href="#1" title="1 sao"></a>
                    <a dt-value="2" id="click-2" href="#2" title="2 sao"></a>
                    <a dt-value="3" id="click-3" href="#3" title="3 sao"></a>
                    <a dt-value="4" id="click-4" href="#4" title="4 sao"></a>
                    <a dt-value="5" id="click-5" href="#5" title="5 sao"></a>
                </div>
            </div>
            <button type="button" class="btn btn-danger btn-vote-user-submit" data-user-id="" data-vote="5" >Đánh giá</button>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>   
    <script>
        let mess = document.getElementById('messenger');
        if(mess) {
            setTimeout(function() {
                mess.style.top = '-80px';
            }, 2500);
        }

        // show job
        $('.show-job').click(function() {
            let id = $(this).closest('tr').attr('data-id-job');
            $.ajax({
                url: '../database/ajax_show_job.php',
                dataType: 'text',
                data: {'id': id},
                success: function(result) {
                    if(result) {
                        $('#showJob').html(result);
                        $('#showJob').css('transform', 'scale(1)');
                    }
                    else{
                        alert('Đã có lỗi xảy ra');
                    }
                }
            });
        });

        // show user
        $('.show-user').click(function() {
            let id = $(this).closest('tr').attr('data-id-user');
            let id_job = $(this).closest('tr').attr('data-notif-id');
            $.ajax({
                url: '../database/ajax_show_user.php',
                dataType: 'text',
                data: {'id': id, 'id_job': id_job},
                success: function(result) {
                    if(result) {
                        $('#showJob').html(result);
                        $('#showJob').css('transform', 'scale(1)');
                    }
                    else{
                        alert('Đã có lỗi xảy ra');
                    }
                }
            });
        });

        $('body').on('click', '.btn-close-job', function() {
            $(this).closest('#showJob').css('transform', 'scale(0)');
        });
        $('body').on('click', '.btn-close-vote-job', function() {
            $(this).closest('#showVoteJob').css('transform', 'scale(0)');
        });

        // vote star job
        $('.btn-vote').click(function(){
            let id_job = $(this).closest('tr').attr('data-id-job');
            let status = $(this).closest('tr').attr('data-notif-status');
            $('.btn-vote-submit').attr('data-job-id', id_job);            
            if(status == 3) {
                $('#showVoteJob').css('transform', 'scale(1)');
                $('#hover-1').click(function() {
                    $('#star_rate_job').css('width', '20%');
                    $('.btn-vote-submit').attr('data-vote', '1');
                });
                $('#hover-2').click(function() {
                    $('#star_rate_job').css('width', '40%');
                    $('.btn-vote-submit').attr('data-vote', '2');
                });
                $('#hover-3').click(function() {
                    $('#star_rate_job').css('width', '60%');
                    $('.btn-vote-submit').attr('data-vote', '3');
                });
                $('#hover-4').click(function() {
                    $('#star_rate_job').css('width', '80%');
                    $('.btn-vote-submit').attr('data-vote', '4');
                });
                $('#hover-5').click(function() {
                    $('#star_rate_job').css('width', '100%');
                    $('.btn-vote-submit').attr('data-vote', '5');
                });
            }
            else {
                alert('Không thể đánh giá sao khi công việc chưa được hoàn thành!');
            }
        });
        // submit vote star job
        $('.btn-vote-submit').click(function() {
            let id_job = $(this).attr('data-job-id');
            let star = $(this).attr('data-vote');
            $.ajax({
                url: '../database/ajax_vote_job.php',
                dataType: 'text',
                data: {'id_job': id_job, 'star': star},
                success: function(result) {
                    if(result) {
                        $('#showVoteJob').css('transform', 'scale(0)');
                        $('.job-voted-'+id_job).html(`<div class="star-rating">
                            <div class="star-base">
                                <div class="star-rate" style="width:${result}%"></div>
                                <a dt-value="1" id="hover-1" href="#1" title="1 sao"></a>
                                <a dt-value="2" id="hover-2" href="#2" title="2 sao"></a>
                                <a dt-value="3" id="hover-3" href="#3" title="3 sao"></a>
                                <a dt-value="4" id="hover-4" href="#4" title="4 sao"></a>
                                <a dt-value="5" id="hover-5" href="#5" title="5 sao"></a>
                            </div>
                        </div>`);                                          
                        $('#messenger_change').addClass('active');
                        setTimeout(function() {
                            $('#messenger_change').removeClass('active');
                        }, 2500);              
                    }
                    else{
                        alert('Đã có lỗi xảy ra');
                    }
                }
            });                
        });

        // change status job
        $('.change-status').change(function(){
            let id = $(this).closest('tr').attr('data-notif-id');
            let status = $(this).val();
            let notif = $('.change-notif').html();
            if(notif == '') {
                notif = 0;
            }
            let notif_number = parseInt(notif);
            if(notif == 0) {
                if(status == 1) {
                    $('.change-notif').html('1');
                    $('.change-notif').addClass('notifications');
                    $('.change-notif').css('display', 'block');
                }
            }
            else {
                if(status == 1) {
                    $('.change-notif').html(notif_number + 1);
                    $('.change-notif').addClass('notifications');
                    $('.change-notif').css('display', 'block');
                }
                else {
                    $('.notifications').html(notif_number - 1);
                    if(notif_number - 1 <= 0) {
                        $('.change-notif').css('display', 'none');
                        $('.change-notif').removeClass('notifications');
                    }
                    else {
                        $('.change-notif').css('display', 'block');
                        $('.change-notif').addClass('notifications');
                    }
                }
            }
            $.ajax({
                url: '../database/ajax_change_status_notification.php',
                dataType: 'text',
                data: {'id': id, 'status': status},
                success: function(result) {
                    if(result) {
                        $('#messenger_change').addClass('active');                        
                        setTimeout(function() {
                            $('#messenger_change').removeClass('active');
                        }, 2500);
                    }
                    else{
                        alert('Đã có lỗi xảy ra');
                    }
                }
            });
        });


        $('body').on('click', '.btn-close-vote-user', function() {
            $(this).closest('#showVoteUser').css('transform', 'scale(0)');            
        });
        // change star user
        $('#click-1').click(function() {
            $('#star_rate_user').css('width', '20%');
            $('.btn-vote-user-submit').attr('data-vote', '1');
        });
        $('#click-2').click(function() {
            $('#star_rate_user').css('width', '40%');
            $('.btn-vote-user-submit').attr('data-vote', '2');
        });
        $('#click-3').click(function() {
            $('#star_rate_user').css('width', '60%');
            $('.btn-vote-user-submit').attr('data-vote', '3');
        });
        $('#click-4').click(function() {
            $('#star_rate_user').css('width', '80%');
            $('.btn-vote-user-submit').attr('data-vote', '4');
        });
        $('#click-5').click(function() {
            $('#star_rate_user').css('width', '100%');
            $('.btn-vote-user-submit').attr('data-vote', '5');
        });

        $('body').on('click', '#showAverage', function() {
            $('#showJob').css('transform', 'scale(0)');
            $('#showVoteUser').css('transform', 'scale(1)');
            $('.btn-vote-user-submit').attr('data-user-id', $(this).attr('data-user-id'));
            $('.btn-vote-user-submit').attr('data-notif-id', $(this).attr('data-notif-id'));
        });
        // vote star user
        $('.btn-vote-user-submit').click(function() {
            let user_id = $(this).attr('data-user-id');
            let notif_id = $(this).attr('data-notif-id');
            let star = $(this).attr('data-vote');
            $.ajax({
                url: '../database/ajax_vote_user.php',
                dataType: 'text',
                data: {
                    'user_id': user_id,
                    'notif_id': notif_id,
                    'star': star
                },
                success: function(result) {
                    if(result) {
                        $('#showVoteUser').css('transform', 'scale(0)');                                         
                        $('#messenger_change').addClass('active');
                        setTimeout(function() {
                            $('#messenger_change').removeClass('active');
                        }, 2500);              
                    }
                    else{
                        alert('Đã có lỗi xảy ra');
                    }
                }
            }); 
        })

        //change status
        $('.label-act').each(function(){
            $(this).click(function(e){
                e.preventDefault();
                if($(this).find('.bg, .button').hasClass('bg-unact') == true){
                    $(this).find('.bg').removeClass('bg-unact');
                    $(this).find('.bg').addClass('bg-act');
                    $(this).find('.button').css({
                        'position': 'absolute',
                        'top': '0',
                        'bottom': '0',
                        'left': '20px',
                        'width': '20px',
                        'height': '20px',
                        'background': '#fff',
                        'border-radius': '45%',
                        'transition': 'all 0.2s ease-in-out'
                    });
                    $(this).find('input').attr('checked', true);
                    $(this).find('input').attr('value', '1');
                }
                else{
                    $(this).find('.bg').removeClass('bg-act');
                    $(this).find('.bg').addClass('bg-unact');
                    $(this).find('.button').css({
                        'position': 'absolute',
                        'top': '0',
                        'bottom': '0',
                        'left': '0px',
                        'width': '20px',
                        'height': '20px',
                        'background': '#fff',
                        'border-radius': '45%',
                        'transition': 'all 0.2s ease-in-out'
                    });
                    $(this).find('input').attr('checked', false);
                    $(this).find('input').attr('value', '0');
                }
                let id = $(this).closest('tr').attr('data-id-job');
                let status = $(this).find('input').attr('value');
                $.ajax({
                    url: '../database/ajax_change_status_job.php',
                    dataType: 'text',
                    data: {
                        'id': id,
                        'status': status
                    },
                    success: function(result) {
                        if(result) {
                            $('#messenger_change').addClass('active');
                            setTimeout(function() {
                                $('#messenger_change').removeClass('active');
                            }, 2000);              
                        }
                        else{
                            alert('Đã có lỗi xảy ra');
                        }
                    }
                });
            });
        });
    </script>

</body>
</html>

<?php
   unset($_SESSION['success']);
?>