<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

//get category
$category_qr = "SELECT * FROM categories";
$category = mysqli_query($link, $category_qr);

$user_id = $_SESSION["id"];
$name = $position = $latlng = $lat = $lng = $salary =  $datetime = $description = "";
$name_err = $position_err = $salary_err = $time_err = $date_err = $description_err = "";

if(isset($_POST['btn_add_job'])) {
    // check name
    if(empty($_POST['txt_position'])){
        $name_err = "Bạn chưa nhập tên công việc";
    }
    else {
        $name = utf8convert($_POST["txt_name"]);
    }

    // check position
    if(empty($_POST['txt_position'])){
        $position_err = "Bạn chưa nhập địa điểm công việc";
    }
    else {
        $position = utf8convert($_POST["txt_position"]);
    }

    // check lat long
    if(!empty($_POST['txt_latlng'])){
        $latlng = trim($_POST['txt_latlng'], '()');
        $latlng = explode(', ', $latlng);
        $lat = $latlng[0];
        $lng = $latlng[1];
    }

    // check salary
    if(empty($_POST['txt_salary'])){
        $salary_err = "Bạn chưa nhập lương";
    }
    else {
        $salary = $_POST["txt_salary"];
    }

    // check time
    if(empty($_POST['txt_time'])){
        $time_err = "Bạn chưa chọn giờ";
    }
    else {
        if(empty($_POST['txt_date'])) {
            $date_err = "Bạn chưa chọn ngày";
        }
        else{
            $date = explode('-',$_POST["txt_date"]);
            $datetime = $_POST["txt_time"] . ' ' . $date[2] .'/'. $date[1] .'/'. $date[0] ;
        }
    }

    // check time
    if(empty($_POST['txt_description'])){
        $description_err = "Bạn chưa nhập mô tả";
    }
    else {
        $description = utf8convert($_POST["txt_description"]);
    }

    $category_id = $_POST["txt_category"];

    if($name != '' && $position != '' && $salary != '' && $description != '' && $datetime != '') {
        $qr = "INSERT INTO locations VALUES ('NULL', '$name', '$position', '$salary', '$lat', '$lng', '$description', '$datetime', '$user_id', '$category_id', '1', 'NULL', 'NULL')";
        mysqli_set_charset($link, "utf8");
        mysqli_query($link, $qr);

        $_SESSION["success"] = 'Thêm mới thành công !';

        header("location: ../map/welcome.php");
    }
}

// show notifications
$id = $_SESSION["id"];
$qr_set = "SELECT notifications.status FROM locations, notifications, users
            WHERE locations.id = notifications.location_id AND notifications.user_id_get = users.id AND locations.user_id = $id GROUP BY notifications.id DESC";
$list_set = mysqli_query($link, $qr_set);
$count_notif = 0;
while($row = mysqli_fetch_array($list_set)) {
    if($row['status'] == 1) {
        $count_notif++;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thêm công việc </title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 48%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            position: fixed !important;
            height: 33px !important;
            border: 1px solid #ced4da;
            border-radius: 3px;
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: .375rem .75rem;
            text-overflow: ellipsis;
            width: 95% !important;
            left: 0 !important;
            top: 49% !important;
            z-index: 10 !important;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }
        #target {
            width: 345px;
        }
        .form-group{
            padding: 0 10px;
            margin-bottom: 10px;
        }
        form{
            margin-top: <?= $position_err == '' ? '50px' : '65px' ?>;
        }
        .group-btn{
            display: flex;
            align-items: center;
            justify-content: space-between;
        }
        .btn-add{
            background-color: #009688;
            color: #fff;
            width: 48%;
        }
        .width-48{
            width: 48%;
        }
        input{
            text-indent: 0;
        }
        .danhmuc a{
            padding: 0 5px;
        }
        .label-title{
            font-size: 12px;
        }
        .help-block{
            position: absolute;
            margin: 40px 0 0 12px !important;
            font-size: 12px;
            color: red;
        }
        @media (max-width: 375px){
            #map {
                height: 52%;
            }
            #pac-input{
                top: 53% !important;
            }
        }
        @media (max-width: 360px){
            #map {
                height: 45%;
            }
            form{
                margin-top: 42px;
            }
            #pac-input{
                top: 46% !important;
                width: 94% !important;
                padding: 0.375rem 0.5rem;
                height: unset !important;
            }
            .form-control{
                padding: 0.275rem 0.5rem;
            }
            .form-group{
                margin-bottom: 8px;
            }
        }
        @media (max-width: 320px){
            #map {
                height: 38%;
            }
            #pac-input{
                top: 39% !important;
                width: 93% !important;
            }
        }
        select.form-control:not([size]):not([multiple]){
            height: calc(2rem + 2px);
        }
    </style>
</head>
<body>
<input id="pac-input" class="controls" type="text" placeholder="Nhập địa điểm công việc " style="width: 100%;height: 30px;">
<div id="map"></div>
<?php if($position_err != '') : ?>
    <span class="help-block"><?= $position_err ?></span>
<?php endif; ?>
<form action="" method="post">
    <input type="hidden" id="input_position" name="txt_position" value="">
    <input type="hidden" id="input_latlng" name="txt_latlng" value="">
    <div class="form-group group-btn">
        <div class="width-48">
            <input type="text" id="name" name="txt_name" class="form-control" placeholder="Tên công việc" required>
        </div>
        <div class="width-48">
            <select name="txt_category" class="form-control">
                <?php while($datas = mysqli_fetch_array($category)) { ?>
                    <option value="<?= $datas['id'] ?>"><?= $datas['name_category'] ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <input onkeyup="formatNumber()" type="text" id="luong" name="txt_salary" class="form-control" placeholder="Mức lương" required>
    </div>
    <div class="form-group group-btn">
        <div class="width-48">
            <lable class="label-title">Vào lúc: </lable>
            <input type="time" id="time" name="txt_time" class="form-control" width="90%" required>
        </div>
        <div class="width-48">
            <lable class="label-title">Ngày: </lable>
            <input type="date" id="date" name="txt_date" class="form-control" width="90%" required>
        </div>
    </div>
    <div class="form-group">
        <textarea id="mo_ta" name="txt_description"  class="form-control"  rows="2" cols="50" placeholder="Mô tả công việc" required></textarea>
    </div>
    <div class="form-group group-btn">
        <button type="submit" name="btn_add_job" class="form-control btn-add">Thêm</button>
        <a href="../map/welcome.php" class="form-control width-48 btn btn-danger">Quay lại</a>
    </div>
</form>
<div class="footer">
    <ul class="danhmuc">
        <li><a href="../map/welcome.php">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Home</span>
        </a></li>
        <li><a href="../vieclam/themmoi.php">
            <i class="fa fa-plus" aria-hidden="true"></i>
            <span>Thêm việc</span>
        </a></li>
        <li style="position: relative;"><a href="../vieclam/danhsach.php">
            <i class="fa fa-list" aria-hidden="true"></i>
            <span>Danh sách</span>
            <div class="<?= $count_notif != 0 ? 'notifications' : '' ?>"><?= $count_notif != 0 ? $count_notif : '' ?></div>
        </a></li>
        <li><a href="../user/profile.php">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span>Hồ sơ</span>
        </a></li>
        <li><a href="../user/change_password.php">
            <i class="fa fa-key" aria-hidden="true"></i>
            <span>Đổi mật khẩu</span>
        </a></li>
    </ul>
</div>
<script>
    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 21.0228161, lng: 105.801944},
            zoom: 12,
            mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {

                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }

                // lấy vị trí
                // console.log(place.geometry.location);
                let position = document.getElementById('pac-input').value;
                document.getElementById('input_position').value = position;
                document.getElementById('input_latlng').value = place.geometry.location;
            });
            map.fitBounds(bounds);
        });
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDX4sSXD8RHHuDN0AwoECksIURo62g7YrY&libraries=places&callback=initAutocomplete"
        async defer></script>
<script>
    function formatNumber() {
        let luong = document.getElementById('luong');
        let amount = luong.value;
        aa = amount.replace(/\./g,"");
        amount = aa.replace(/\,/g,"");
        if (!Number.isInteger(amount)) {
            value = Intl.NumberFormat().format(amount);
            luong.value = value;
            console.log(typeof value);
        }
    };
</script>

</body>
</html>