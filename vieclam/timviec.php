<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Danh sách công việc</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="../css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/font-awesome.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper {
            position: absolute;
            left: 0;
            top: 0;
            width: 85%;
        }

        .wrapper ul li {
            width: 100%;
            display: block;
            height: 34px;
            border-bottom: 1px solid;
            line-height: 34px;
        }
        input.form-control{
            width: 85%;
            margin: auto;
        }
        .wrapper h5 {
            font-size: 13px;
            font-family: sans-serif;
            color: darkred;
            text-transform: uppercase;
            padding: 7px;
        }
    </style>
</head>
<body>

<div class="wrapper">

    <!-- Search form -->
    <div class="md-form" style="padding-top: 10px;">
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
    </div>
    <ul style="    overflow: scroll;
                    height: 679px;">
        <h5><i class="fa fa-list-alt" aria-hidden="true"></i>
            Danh sách công việc cho bạn</h5>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 2</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 3</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 2</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 3</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 2</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 3</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
        <li><a href="#"><i class="fa fa-thumb-tack" aria-hidden="true"></i>
                Việc làm 1</a></li>
    </ul>
</div>
<div class="footer">
    <ul class="danhmuc">
        <li><a href="timviec.php"><i class="fa fa-search" aria-hidden="true"></i>
                <span>Tìm Việc</span>
            </a></li>
        <li><a href="#"><i class="fa fa-blind" aria-hidden="true"></i>
                <span>Tìm thợ</span> </a></li>
        <li><a href="#"><i class="fa fa-car" aria-hidden="true"></i>
                <span>Vận chuyển</span> </a></li>
        <li><a href="../map/welcome.php"> <i class="fa fa-map" aria-hidden="true"></i>
                <span>Map</span></a></li>
        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>
                <span>Hồ sơ</span></a></li>
    </ul>
</div>
</body>
</html>