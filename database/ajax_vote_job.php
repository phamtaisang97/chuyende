<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

$id = $_SESSION["id"];
$id_job = $_GET["id_job"];
$star = (int) $_GET["star"];
$vote_star = 100;
if($star == 1) $vote_star = 20;
if($star == 2) $vote_star = 40;
if($star == 3) $vote_star = 60;
if($star == 4) $vote_star = 80;
if($star == 5) $vote_star = 100;

$qr_job = "SELECT * FROM locations WHERE id = $id_job";
$job = mysqli_fetch_array(mysqli_query($link, $qr_job));

$vote_average = (int) $job['vote_star'] + $vote_star;
$vote_count = (int) $job['vote_count'] + 1;

$qr_vote_notif = "
    UPDATE notifications SET
    vote_star_user_get = '$vote_star'
    WHERE location_id = '$id_job' AND user_id_get = '$id'
";
mysqli_query($link, $qr_vote_notif);

$qr = "
    UPDATE locations SET
    vote_star = '$vote_average',
    vote_count = '$vote_count'
    WHERE id = '$id_job'
";
mysqli_query($link, $qr);

$user_id = $job['user_id'];
$qr_all_job = "SELECT SUM(vote_star) AS total_star, SUM(vote_count) AS total_count FROM locations WHERE user_id = $user_id";
$star_user = mysqli_fetch_array(mysqli_query($link, $qr_all_job));

$vote_star_update = (int) $star_user['total_star'] / (int) $star_user['total_count'];
$qr_user = "
    UPDATE users SET
    vote_star = '$vote_star_update'
    WHERE id = '$user_id'
";
mysqli_query($link, $qr_user);

echo $vote_star;