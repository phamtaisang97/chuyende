<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

$id = $_SESSION["id"];
$user_id = $_GET["user_id"];
$notif_id = $_GET["notif_id"];
$star = (int) $_GET["star"];
$vote_star = 100;
if($star == 1) $vote_star = 20;
if($star == 2) $vote_star = 40;
if($star == 3) $vote_star = 60;
if($star == 4) $vote_star = 80;
if($star == 5) $vote_star = 100;

$qr_vote_notif = "
    UPDATE notifications SET
    vote_star_user_boss = '$vote_star'
    WHERE id = '$notif_id'
";
mysqli_query($link, $qr_vote_notif);

$qr_user = "SELECT * FROM users WHERE id = $user_id";
$user = mysqli_fetch_array(mysqli_query($link, $qr_user));

if($user['vote_star'] == null || (int) $user['vote_star'] == 0) {
    $vote_star_update = $vote_star;
}
else {
    $vote_star_update = (int) ($user['vote_star'] + $vote_star) / 2;
}
$qr_user_update = "
    UPDATE users SET
    vote_star = '$vote_star_update'
    WHERE id = '$user_id'
";
mysqli_query($link, $qr_user_update);

echo 'ok';