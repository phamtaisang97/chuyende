<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

$id = $_GET["id"];
$qr_job = "SELECT * FROM locations WHERE id = $id";
$job = mysqli_fetch_array(mysqli_query($link, $qr_job));
$user_id = $job['user_id'];
$qr_user = "SELECT * FROM users WHERE id = $user_id";
$user = mysqli_fetch_array(mysqli_query($link, $qr_user));

if($job['vote_star'] == 0) {
    echo '<div class="show-job-detail">
            <div class="btn-close-job"><i class="fa fa-times-circle"></i></div>
            <h5>Công việc</h5>
            <div>Tên công việc: '.$job['name'].'</div>
            <div>Thời gian: '.$job['time_work'].'</div>
            <div>Địa điểm: '.$job['address'].'</div>
            <div>Lương: '.$job['price'].' VNĐ</div>
            <div>Mô tả cv: '.$job['description'].'</div>
            <div>Người đăng: '.$user['fullname'].'</div>
            <div>SĐT: <a href="tel: '.$user['contact'].'" title="">'.$user['contact'].'</a></div>
            <div>Email: <a href="mailto: '.$user['email'].'" title="">'.$user['email'].'</a></div>
            <div>Đánh giá chung: Chưa có đánh giá nào</div>
        </div>';
}
else {
    echo '<div class="show-job-detail">
            <div class="btn-close-job"><i class="fa fa-times-circle"></i></div>
            <h5>Công việc</h5>
            <div>Tên công việc: '.$job['name'].'</div>
            <div>Thời gian: '.$job['time_work'].'</div>
            <div>Địa điểm: '.$job['address'].'</div>
            <div>Lương: '.$job['price'].' VNĐ</div>
            <div>Mô tả cv: '.$job['description'].'</div>
            <div>Người đăng: '.$user['fullname'].'</div>
            <div>SĐT: <a href="tel: '.$user['contact'].'" title="">'.$user['contact'].'</a></div>
            <div>Email: <a href="mailto: '.$user['email'].'" title="">'.$user['email'].'</a></div>
            <div class="d-flex">
                <span>Đánh giá:</span> 
                <div class="star-rating">
                    <div class="star-base">
                        <div class="star-rate" style="width:'.$job['vote_star'] / $job['vote_count'].'%"></div>
                        <a dt-value="1" href="#1"></a>
                        <a dt-value="2" href="#2"></a>
                        <a dt-value="3" href="#3"></a>
                        <a dt-value="4" href="#4"></a>
                        <a dt-value="5" href="#5"></a>
                    </div>
                </div>
                <p> &nbsp;('.$job['vote_count'].' đánh giá)</p>
            </div>
        </div>';
}