<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: ../login/dangnhap.php");
    exit;
}

// Include config file
require_once "../database/db.php";

$id = $_GET["id"];
$id_notif = $_GET["id_job"];
$qr_user = "SELECT * FROM users WHERE id = $id";
$user = mysqli_fetch_array(mysqli_query($link, $qr_user));

$qr_notif = "SELECT * FROM notifications WHERE id = $id_notif";
$notif = mysqli_fetch_array(mysqli_query($link, $qr_notif));


//count_vote
$qr_vote_notif = "SELECT COUNT(*) as total_vote FROM notifications WHERE user_id_boss = $id AND vote_star_user_boss > 0";
$count_vote_notif = mysqli_fetch_array(mysqli_query($link, $qr_vote_notif));
$qr_vote_location = "SELECT SUM(vote_count) as total_vote FROM locations WHERE user_id = $id";
$count_vote_location = mysqli_fetch_array(mysqli_query($link, $qr_vote_location));
$total_vote = (int) $count_vote_notif['total_vote'] + (int) $count_vote_location['total_vote'];

if($user['vote_star'] == 0) {
    echo '<div class="show-job-detail">
            <div class="btn-close-job"><i class="fa fa-times-circle"></i></div>
            <h5>Thông tin người nhận</h5>
            <div>Người đăng: '.$user['fullname'].'</div>
            <div>SĐT: <a href="tel: '.$user['contact'].'" title="">'.$user['contact'].'</a></div>
            <div>Email: <a href="mailto: '.$user['email'].'" title="">'.$user['email'].'</a></div>
            <div>Địa chỉ: '.$user['address'].'</div>
            <div>Đánh giá chung: <span style="color: #ead416;">Chưa có đánh giá nào</span></div>                
            <div>Đánh giá của bạn: <span style="color: #ead416;">Bạn chưa đánh giá</span></div>
            <button id="showAverage" style="margin-top: 5px;" type="button" class="btn btn-danger" data-user-id="'.$id.'" data-notif-id="'.$id_notif.'">Đánh giá ngay</button>              
        </div>';
}
else {
    if($notif['vote_star_user_boss'] == 0) {
        echo '<div class="show-job-detail">
                <div class="btn-close-job"><i class="fa fa-times-circle"></i></div>
                <h5>Thông tin người nhận</h5>
                <div>Người đăng: '.$user['fullname'].'</div>
                <div>SĐT: <a href="tel: '.$user['contact'].'" title="">'.$user['contact'].'</a></div>
                <div>Email: <a href="mailto: '.$user['email'].'" title="">'.$user['email'].'</a></div>
                <div>Địa chỉ: '.$user['address'].'</div>
                <div class="d-flex">
                    <span>Đánh giá:</span> 
                    <div class="star-rating">
                        <div class="star-base">
                            <div class="star-rate" style="width:'.$user['vote_star'].'%"></div>
                            <a dt-value="1" href="#1"></a>
                            <a dt-value="2" href="#2"></a>
                            <a dt-value="3" href="#3"></a>
                            <a dt-value="4" href="#4"></a>
                            <a dt-value="5" href="#5"></a>
                        </div>
                    </div>
                    <p> &nbsp;('.$total_vote.' đánh giá)</p>
                </div>               
                <div>Đánh giá của bạn: <span style="color: #ead416;">Bạn chưa đánh giá</span></div>
                <button id="showAverage" style="margin-top: 5px;" type="button" class="btn btn-danger" data-user-id="'.$id.'" data-notif-id="'.$id_notif.'">Đánh giá ngay</button>              
            </div>';
    }
    else {
        echo '<div class="show-job-detail">
                <div class="btn-close-job"><i class="fa fa-times-circle"></i></div>
                <h5>Thông tin người nhận</h5>
                <div>Người đăng: '.$user['fullname'].'</div>
                <div>SĐT: <a href="tel: '.$user['contact'].'" title="">'.$user['contact'].'</a></div>
                <div>Email: <a href="mailto: '.$user['email'].'" title="">'.$user['email'].'</a></div>
                <div>Địa chỉ: '.$user['address'].'</div>
                <div class="d-flex">
                    <span>Đánh giá:</span> 
                    <div class="star-rating">
                        <div class="star-base">
                            <div class="star-rate" style="width:'.$user['vote_star'].'%"></div>
                            <a dt-value="1" href="#1"></a>
                            <a dt-value="2" href="#2"></a>
                            <a dt-value="3" href="#3"></a>
                            <a dt-value="4" href="#4"></a>
                            <a dt-value="5" href="#5"></a>
                        </div>
                    </div>
                    <p> &nbsp;('.$total_vote.' đánh giá)</p>
                </div>
                <div class="d-flex">
                    <span>Đánh giá của bạn:</span> 
                    <div class="star-rating">
                        <div class="star-base">
                            <div class="star-rate" style="width:'.$notif['vote_star_user_boss'].'%"></div>
                            <a dt-value="1" href="#1"></a>
                            <a dt-value="2" href="#2"></a>
                            <a dt-value="3" href="#3"></a>
                            <a dt-value="4" href="#4"></a>
                            <a dt-value="5" href="#5"></a>
                        </div>
                    </div>
                </div>
            </div>';
    }
}