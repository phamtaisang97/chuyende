-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2020 at 10:27 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `map_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name_category` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_category`) VALUES
(1, 'Bung be'),
(2, 'Chuyen nha'),
(3, 'Chay tiec/su kien');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `address` varchar(255) NOT NULL,
  `price` varchar(15) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lon` float(10,6) NOT NULL,
  `description` text NOT NULL,
  `time_work` varchar(255) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `vote_star` int(3) DEFAULT NULL,
  `vote_count` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `address`, `price`, `lat`, `lon`, `description`, `time_work`, `user_id`, `category_id`, `status`, `vote_star`, `vote_count`) VALUES
(38, 'Chay tiec', 'Dai Hoc Cong Nghiep Ha Noi, Pho Nhon, Nhon, Minh Khai, Tu Liem, Ha Noi, Viet Nam', '250.000', 21.053795, 105.734459, 'Lam vo van thoi', '07:30 15/06/2020', 4, 1, 1, 60, 1),
(39, 'Chuyen nha', 'My Dinh, Tu Liem, Ha Noi, Viet Nam', '350.000', 21.023472, 105.773262, 'Chuyen nha dan tu trung cu nay sang chung cu kia', '08:00 18/06/2020', 5, 2, 1, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `user_id_get` int(11) NOT NULL,
  `user_id_boss` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` int(15) NOT NULL,
  `vote_star_user_get` int(3) DEFAULT NULL,
  `vote_star_user_boss` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `location_id`, `user_id_get`, `user_id_boss`, `status`, `created_at`, `vote_star_user_get`, `vote_star_user_boss`) VALUES
(29, 38, 5, 4, 3, 1592034283, 60, 100),
(30, 39, 4, 5, 1, 1592036482, 0, 0),
(31, 38, 7, 4, 2, 1592036647, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `fullname` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `fullname_khongdau` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `birthday` varchar(20) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `contact` varchar(10) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `vote_star` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `fullname`, `fullname_khongdau`, `birthday`, `contact`, `email`, `address`, `description`, `vote_star`) VALUES
(1, 'admin@gmail.com', '$2y$10$Tb0VCeF4Ri4iu6zI9vy31eFPoSXUh6ZBCm0HRiraebZ4ebnX6eF/u', '2020-05-25 15:53:19', 'admin', 'admin', '', '', '', '', '', NULL),
(3, 'sang', '$2y$10$jarmAte8Gh.QodzyhTnfqeIfDIZ7Zl2ceIrDqnd/iy1PbforbJESK', '2020-05-25 16:02:16', 'admin', 'admin', '', '', '', '', '', NULL),
(4, 'hung', '$2y$10$1Qezk4J93EaNKMAsUMpIMu/wEpyI9NGaISC8Ws5KFLKwZxr/ezIQe', '2020-05-26 08:52:45', 'Quách Duy Hưng', 'Quach Duy Hung', '03/11/1998', '0377565340', 'quachduyhung.1998@gmail.com', 'Số 59, thôn Thúy Hội, xã Tân Hội, huyện Đan Phượng, Hà Nội', 'Tổng giám đốc công ty technology one member', 100),
(5, 'admin', '$2y$10$OB.BTi0qx./AP8RNQ8I9T.GrE74W.QLFAVnGP7HpyhA.0hJ/VGf.G', '2020-06-03 17:52:51', 'Tai Sang', 'Tai Sang', '01/01/2020', '0966614689', 'admin@admin.com', 'Duy Tân , Hà Nội', '', NULL),
(7, 'abc', '$2y$10$VZt2y.uLbsaJFw3QDI8IDewG4L.EKKLEHOLd47culrEQ/.AQm2d.6', '2020-06-13 15:23:29', 'Nguyễn Văn ABC', 'Nguyen Van ABC', '13/06/2000', '', '', '', 'vô danh tiểu tốt', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
