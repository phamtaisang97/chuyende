<?php

require 'db.php';
session_start();
try {

    $db = new PDO($dsn, $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $id_user = $_SESSION["id"];
    $sth = $db->query("SELECT * FROM wishlist,locations where wishlist.id_location = locations.id and wishlist.id_user = $id_user");
    $categories = $sth->fetchAll();
    echo json_encode($categories);

} catch (Exception $e) {
    echo $e->getMessage();
}

