<?php

require 'db.php';

try {

    $db = new PDO($dsn, $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sth = $db->query("SELECT * FROM categories");
    $categories = $sth->fetchAll();

    echo json_encode($categories);

} catch (Exception $e) {
    echo $e->getMessage();
}

